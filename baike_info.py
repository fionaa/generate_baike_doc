#-*-coding:utf-8-*-
import re
import requests
import bs4
from bs4 import BeautifulSoup
import json
import codecs
import sys
import os
from get_animal_name import animal_data
from  docx import  Document
from  docx.shared import  Pt
from  docx.oxml.ns import  qn
from docx.shared import Inches

path = sys.path[0] + os.sep
headers = {
    "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding":"gzip, deflate, br",
    "Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-HK;q=0.7,en-US;q=0.6",
    "Connection":"keep-alive",
    "Cookie":"BAIDUID=12D740BD92DEA90B607F5B827987F30E:FG=1; BIDUPSID=12D740BD92DEA90B607F5B827987F30E; PSTM=1534166632; BKWPF=3; BDUSS=lleW52cG9MalVYcUhKeWJSYllpMlgzQXpnN2lORml-UXh3b1BqRGpqSnBtcVJiQVFBQUFBJCQAAAAAAAAAAAEAAAARJts6wu3D98flt-cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGkNfVtpDX1bT1; PSINO=1; H_PS_PSSID=1447_21105_20882_26350_26924_20927; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; pgv_pvi=419963904; pgv_si=s2644193280; Hm_lvt_55b574651fcae74b0a9f1cf9c8d7c93a=1534920932,1535362634,1535362645,1535362662; Hm_lpvt_55b574651fcae74b0a9f1cf9c8d7c93a=1535362662",
    "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36", 
    "Host": 'baike.baidu.com',
    "Upgrade-Insecure-Requests":"1"
}

def craw(url, item):
    html = requests.get(url, headers=headers).content
    data = dict()
    data['url'] = url
    data['name'] = item
    soup = BeautifulSoup(html, 'lxml')
    title = soup.find('h2').get_text()
    data['abstract'] = soup.find('div', class_='lemma-summary').get_text().strip().replace('\n', '').replace('\t', '')
    basic_info = soup.find('div', class_='basic-info')
    dts = basic_info.find_all('dt', class_='name')
    dds = basic_info.find_all('dd', class_='value')
    data['basic_info'] = dict()
    for i in range(len(dts)):
        name = dts[i].get_text().strip().replace('\n', '').replace('\t', '')
        value = dds[i].get_text().strip().replace('\n', '').replace('\t', '')
        data['basic_info'][name] = value
    paras = soup.find_all('div', class_=['para-title', 'para'])
    content = dict()
    # move cursor to div: para-title level-2
    for i in range(len(paras)):
        if 'level-2' in paras[i]['class']:
            paras = paras[i:]
            break
    level3_flag = False
    # traversal content, caution: there is level-3 para, so the code will be more complicate 
    for para in paras:
        if 'level-2' in para['class']:
            prefix = para.span.get_text().strip().replace('\n', '')
            name = para.h2.get_text().strip().replace('\n', '').replace(prefix, '')
            content[name] = ''
            level3_flag = False
        elif 'level-3' in para['class']:
            if not level3_flag:
                content[name] = dict()
            prefix = para.span.get_text().strip().replace('\n', '')
            children = para.h3.get_text().strip().replace('\n', '').replace(prefix, '')
            content[name][children] = ''
            level3_flag = True
        else:
            text = para.get_text().strip().replace('\n', '').replace('\t', '')
            if level3_flag:
                content[name][children] += text
            else:
                content[name] += text
    data['content'] = content
    #打开文档
    document = Document()
    document.add_heading(data['name'],0)
    document.add_heading(u'简介',1)
    paragraph = document.add_paragraph('')
    run = paragraph.add_run(data['abstract']).bold = False
    
    document.add_heading(u'基础信息',1)
    paragraph1 = document.add_paragraph('')
    for k,v in data['basic_info'].items():
        run = paragraph1.add_run(k+':').bold = True
        run = paragraph1.add_run(v + '\n').bold = False

    document.add_heading(u'详细信息',1)
    paragraph2 = document.add_paragraph('')
    for k,v in data['content'].items():
        run = paragraph2.add_run(k+':').bold = True
        if type(v) == dict:
            for kk,vv in v.items():
                run = paragraph2.add_run(kk+':').bold = True
                run = paragraph1.add_run(vv + '\n').bold = False
        else:
            run = paragraph2.add_run(str(v) + '\n').bold = False
    #保存文件
    document.save('{}.docx'.format(data['name']))

    # f = codecs.open(path + 'baike.json', 'w', 'utf-8')
    # json.dump(data, f, ensure_ascii=False)
    # f.write('\n')
    # f.close()

if __name__ == '__main__':
    baseurl = 'http://baike.baidu.com/item/'
    pageNum = 1
    pageSize = 10
    send_req = True
    while send_req:
        return_data = animal_data.get_animal_info(appsecret = "8c03d9c66d6a4ec985d7dab694ab5535", pageNum = pageNum, pageSize=pageSize)
        animal_names = []
        for d in return_data:
            name = d["Chinese_name"]
            find_index = name.find("（")
            find_index_e = name.find("(")
            if find_index != -1:
                name = name[0:find_index]
            if find_index_e != -1:
                name = name[0:find_index_e]
            animal_names.append(name)
        for item in animal_names:
            url = baseurl + item
            print(item)
            try:
                craw(url, item)
                print('Successfully process {}'.format(item))
            except:
                print('Can not process {}'.format(item))
        if return_data:
            pageNum += 1
        else:
            send_req = False