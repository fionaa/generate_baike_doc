# -*- coding: utf-8 -*-
import json
import base64
import logging
import os
import requests


def to_unicode(string):
 
    ret = ''
    for v in string:
        ret = ret + hex(ord(v)).upper().replace('0X', '\\u')
 
    return ret

class AnimalData(object):
    def __init__(self):
        self.url = "http://data.zjzwfw.gov.cn/jdop_front/interfaces/cata_3590/get_data.do"

    def get_animal_info(self, appsecret, pageNum, pageSize):
        try:
            params={
                "appsecret":appsecret,
                "pageNum": pageNum,
                "pageSize": pageSize
            }
            res = requests.get(self.url, params=params)
            if res.status_code != 200:
                logging.info("Get edgenode capture_path info error: {}".format(res.status_code))
                return None
            else:
                res_info = res.json()
                res_value = res_info["data"]
                logging.info("data:{}".format(res_value))
                return res_value
        except Exception as e:
            logging.error(e)
            return None

animal_data = AnimalData()